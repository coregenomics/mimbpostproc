# Pausing indices

```{r Calculate pausing indices from promoter and gene body densities, cache = TRUE}
load("densities.RData")

## Extract densities of gene promoter and gene body into matrix.
densities_mat <- matrix(mcols(unlist(densities[-1, "density_rpkb"]))[[1]],
                        ncol = 2, dimnames = list(NULL, names(densities[-1])))
head(densities_mat)
## Calculate pausing index by dividing promoter by body densities.
pausing_indices <-
    densities_mat[, "promoter"] /
    densities_mat[, "body"]
## Summaries.
str(pausing_indices)
summary(pausing_indices)
summary(pausing_indices[! is.infinite(pausing_indices)])
```
