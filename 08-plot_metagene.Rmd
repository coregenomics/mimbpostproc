# Plot metagene

```{r Plot metagene of both strands, cache = TRUE}
load("reads_h3u.RData")
load("genes_plot.RData")

upstream <- 500
downstream <- 2500
genes_meta <- promoters(genes_plot,
                        upstream = upstream,
                        downstream = downstream)

## Create tiles to generate count matrix.
window_size <- 10
windows <- tile(genes_meta, width = window_size)
n_windows <- length(windows[[1]])

## Create count matrix.
scoreOverlaps <- function(gr, reads) {
    ol <- findOverlaps(gr, reads)
    mcols(ol)$score <- score(reads[to(ol)])
    df <- as(ol, "DataFrame")
    df <- aggregate(score ~ queryHits, df, sum)
    score <- vector("integer", length(gr))
    score[df$queryHits] <- df$score
    score
}
matrixOverlaps <- function(windows, reads) {
    counts_list <- relist(scoreOverlaps(unlist(windows), reads_h3u), windows)
    mat <- as(counts_list, "matrix")
    mat[is.na(mat)] <- 0
    mat
}
mat_p <- matrixOverlaps(windows, reads)
mat_m <- matrixOverlaps(invertStrand(windows), reads)

q <- 0.95
quantile <- matrix(c(apply(mat_p, 2, stats::quantile, q),
                     -apply(mat_m, 2, stats::quantile, q)),
                   nrow = 2, byrow = TRUE)
plot(x = seq(-upstream,
              downstream,
              length.out = n_windows),
     y = quantile[1, ],
     xlim = c(-upstream, downstream),
     ylim = range(quantile),
     col = "red",
     type = "l",
     main = sprintf("Human 3 untreated (%d%% quantile, n = %d)",
                    q * 100, length(genes_meta)),
     xlab = "Distance from TSS (bp)",
     ylab = "Counts")
lines(x = seq(-upstream,
              downstream,
              length.out = n_windows),
      y = quantile[2, ],
      col = "blue",
      type = "l")
```
