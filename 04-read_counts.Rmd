# Read counts

```{r Calculate promoter and gene body densities, cache = TRUE}
load("reads_h3u.RData")
load("genes_active.RData")

genes_long <- genes_active[width(genes_active) > 500]
mcols(genes_long) <- NULL
message("Dropped ", length(genes_active) - length(genes_long), " of ",
        length(genes_active), " genes that were < 500 bp long.")

## Create a function to aggregate read overlaps.  We can't use
## countOverlaps() directly, because we need to use the score().
scoreOverlaps <- function(gr, reads) {
    ol <- findOverlaps(gr, reads)
    mcols(ol)$score <- score(reads[to(ol)])
    df <- as(ol, "DataFrame")
    df <- aggregate(score ~ queryHits, df, sum)
    score <- vector("integer", length(gr))
    score[df$queryHits] <- df$score
    score
}

gene_promoters <- promoters(genes_long,
                            upstream = 0,
                            downstream = 250)
score(gene_promoters) <- scoreOverlaps(gene_promoters, reads_h3u)
gene_promoters
gene_bodies <- psetdiff(genes_long,
                        promoters(genes_long,
                                  upstream = 0,
                                  downstream = 500))
## Aggregate reads that overlap this region.
score(gene_bodies) <- scoreOverlaps(gene_bodies, reads_h3u)
gene_bodies
## Save results.
score(genes_long) <- scoreOverlaps(genes_long, reads_h3u)
counts <- GRangesList(full = genes_long,
                      promoter = gene_promoters,
                      body = gene_bodies)
save(counts, file = "counts.RData")
```
